/**
 * Este programa pide datos al usuario para guardarlos en la variable creada para luego presentarlos en la salida.
 *
 * @author: Alejandro Castro Rivera
 * @date: 15 de marzo de 2021
 */

#include <stdio.h>

int main()
{
    /* Se declara una variable para almacenar un dato entero negativo o positivo */

    int nolevoyacopiarlavariablealprofe;

    /* Se pide el dato al señor usuario para ser almacenado */

    printf("Escriba porfis un valor entero: ");

    /* Se guarda el dato recibido por el señor usuario para su uso proximo */

    scanf("%d",&nolevoyacopiarlavariablealprofe);

    /* Se presenta al señor usuario el dato almacenado con anterioridad */

    printf("El valor entero que uste guardo es: %d\n", nolevoyacopiarlavariablealprofe);

    /* Se declara una variable para almacenar un dato entero positivo  */

    unsigned int unint;

    /* Se pide el dato al señor usuario para ser almacenado */

    printf("Escriba porfis un valor entero positivo: ");

    /* Se guarda el dato recibido por el señor usuario para su uso proximo */

    scanf("%d",&unint);

    /* Se presenta al señor usuario el dato almacenado con anterioridad */

    printf("El valor entero positivo que uste guardo es: %d\n", unint);

        /* Se declara una variable para almacenar un dato entero corto */

    int short numc;

    /* Se le pide al señor usuario que ingrese un numero corto */

    printf("Escriba porfis un numero entero corto: ");

    /* Se guarda el dato recibido por el señor usuario para su uso proximo */

    scanf(" %d", &numc);

    /* Se presenta al señor usuario el dato almacenado con anterioridad */

    printf("El numero corto que uste guardo es: %d\n", numc);

    unsigned int short unnumc;

    /* Se le pide al señor usuario que ingrese un numero positivo corto */

    printf("Escriba porfis un numero entero  positivo corto: ");

    /* Se guarda el dato recibido por el señor usuario para su uso proximo */

    scanf(" %d", &unnumc);

    /* Se presenta al señor usuario el dato almacenado con anterioridad */

    printf("El numero corto positivo que uste guardo es: %d\n", unnumc);


    /* Se declara una variable para almacenar un dato entero largo */

    int long numl;

    /* Se le pide al señor usuario que ingrese un numero entero largo */

    printf("Escriba porfis un numero entero largo: ");

    /* Se guarda el dato recibido por el señor usuario para su uso proximo */

    scanf(" %ld", &numl);

    /* Se presenta al señor usuario el dato almacenado con anterioridad */

    printf("El numero entero largo que uste guardo es: %ld\n", numl);

    unsigned int long unnuml;

    /* Se le pide al señor usuario que ingrese un numero entero largo positivo */

    printf("Escriba porfis un numero entero largo positivo: ");

    /* Se guarda el dato recibido por el señor usuario para su uso proximo */

    scanf(" %ld", &unnuml);

    /* Se presenta al señor usuario el dato almacenado con anterioridad */

    printf("El numero entero largo positivo que uste guardo es: %ld\n", unnuml);

    /* Se declara una variable para almacenar caracteres */

    char nom [20];

    /* Se le pide al señor usuario que ingrese un nombre */

    printf("Escriba porfis un nombre: ");

    /* Se guarda el dato recibido por el señor usuario para su uso proximo */

    scanf(" %s", &nom);

    /* Se presenta al señor usuario el dato almacenado con anterioridad */

    printf("El nombre que uste guardo es:%s\n", nom);

    unsigned char unnom [20];

    /* Se le pide al señor usuario que ingrese un nombre positivo */

    printf("Escriba porfis un nombre positivo: ");

    /* Se guarda el dato recibido por el señor usuario para su uso proximo */

    scanf(" %s", &unnom);

    /* Se presenta al señor usuario el dato almacenado con anterioridad */

    printf("El nombre positivo que uste guardo es:%s\n", unnom);

    /* Se declara una variable para almacenar un dato decimal */

    float numd;

    /* Se le pide al señor usuario que ingrese un numero decimal */

    printf("Escriba porfis un numero decimal: ");

    /* Se guarda el dato recibido por el señor usuario para su uso proximo */

    scanf(" %f", &numd);

    /* Se presenta al señor usuario el dato almacenado con anterioridad */

    printf("El numero decimal que uste guardo es: %f\n", numd);



    /* Se declara una variable para almacenar un decimal largo */

    double db;

    /* Se le pide al señor usuario que ingrese un decimal largo */

    printf("Escriba porfis un numero decimal largo: ");

    /* Se guarda el dato recibido por el señor usuario para su uso proximo */

    scanf(" %lf", &db);

    /* Se presenta al señor usuario el dato almacenado con anterioridad */

    printf("El numero decimal largo que uste guardo es: %lf\n", db);

    /* Se termina este su programa indicando: "finalizado normalmente". Por su atencion gracias */

    return 0;
}
